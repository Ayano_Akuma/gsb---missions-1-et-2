# GSB - Missions 1 et 2

Le laboratoire GSB est le fruit de la fusion entre Galaxy et le Conglomérat Swiss Bourdin. Il souhaite renouveler l’activité de visite médicale. Les visiteurs médicaux ont la charge de démarcher le personnel. On souhaite rapprocher la gestion du suivi des visites. Les déplacements et actions de terrain génèrent des frais pris en charge par la comptabilité.
 
L'application doit permettre d'enregistrer tout frais engagé, aussi bien pour l'activité directe (déplacement, restauration et hébergement) que pour les activités annexes (événementiel, conférences, autres), et de présenter un suivi daté des opérations menées par le service comptable (réception des pièces, validation de la demande de remboursement, mise en paiement, remboursement effectué).
L'application Web destinée aux visiteurs, délégués et responsables de secteur sera en ligne, accessible depuis un ordinateur.
La partie utilisée par les services comptables sera aussi sous forme d'une interface Web.
Le module accessible à la force de visite sera intégré à l'application de gestion des comptes-rendus de visite, mais sous forme d'une interface spécifique (elle ne doit pas être fusionnée à la saisie des CR, elle sera sur un onglet ou une page spécifique).
L'environnement doit être accessible aux seuls acteurs de l'entreprise. 
Une authentification préalable sera nécessaire pour l'accès au contenu. 
 
Dans un premier temps nous avons réalisé la partie connexion d’un comptable et sécurisation des mots de passe, puis nous avons ensuite réalisé la partie validation d’une fiche et suivi du paiement.
 
Pour la réalisation de cette application, les outils suivants ont été nécessaires : NetBeans, WampServer, phpMyAdmin et les langages PHP et SQL
