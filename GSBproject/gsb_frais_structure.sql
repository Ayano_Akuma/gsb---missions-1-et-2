-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Lun 04 Juillet 2011 à 14:08
-- Version du serveur: 5.5.8
-- Version de PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `gsb_frais`
--

-- --------------------------------------------------------

--
-- Structure de la table `FraisForfait`
--

CREATE TABLE IF NOT EXISTS `FraisForfait` (
  `id_FraisForfait` char(3) NOT NULL,
  `libelle` char(20) DEFAULT NULL,
  `montant` decimal(5,2) DEFAULT NULL,
  PRIMARY KEY (`id_FraisForfait`)
) ENGINE=InnoDB;


-- --------------------------------------------------------

--
-- Structure de la table `Etat`
--

CREATE TABLE IF NOT EXISTS `Etat` (
  `id_Etat` char(2) NOT NULL,
  `libelle` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_Etat`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `Comptable`
--

CREATE TABLE IF NOT EXISTS `Comptable` (
  `id_Comptable` char(4) NOT NULL,
  `nom` char(30) DEFAULT NULL,
  `prenom` char(30)  DEFAULT NULL, 
  `login` char(20) DEFAULT NULL,
  `mdp` char(20) DEFAULT NULL,
  `adresse` char(30) DEFAULT NULL,
  `cp` char(5) DEFAULT NULL,
  `ville` char(30) DEFAULT NULL,
  `dateEmbauche` date DEFAULT NULL,
  PRIMARY KEY (`id_Comptable`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `Visiteur`
--

CREATE TABLE IF NOT EXISTS `Visiteur` (
  `id_Visiteur` char(4) NOT NULL,
  `nom` char(30) DEFAULT NULL,
  `prenom` char(30)  DEFAULT NULL, 
  `login` char(20) DEFAULT NULL,
  `mdp` char(20) DEFAULT NULL,
  `adresse` char(30) DEFAULT NULL,
  `cp` char(5) DEFAULT NULL,
  `ville` char(30) DEFAULT NULL,
  `dateEmbauche` date DEFAULT NULL,
  `id_Comptable` char(4) NOT NULL,
  PRIMARY KEY (`id_Visiteur`),
  FOREIGN KEY (`id_Comptable`) REFERENCES Comptable(`id_Comptable`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `FicheFrais`
--

CREATE TABLE IF NOT EXISTS `FicheFrais` (
  `id_FicheFrais` char(4) NOT NULL,
  `mois` char(10) DEFAULT NULL,
  `nbJustificatifs` int(11) DEFAULT NULL,
  `montantValide` decimal(10,2) DEFAULT NULL,
  `dateModif` date DEFAULT NULL,
  `id_Etat` char(2) DEFAULT 'CR',
  `id_Visiteur` char(4) NOT NULL,
  PRIMARY KEY (`id_FicheFrais`),
  FOREIGN KEY (`id_Etat`) REFERENCES Etat(`id_Etat`),
  FOREIGN KEY (`id_Visiteur`) REFERENCES Visiteur(`id_Visiteur`)
) ENGINE=InnoDB;


-- --------------------------------------------------------

--
-- Structure de la table `LigneFraisForfait`
--

CREATE TABLE IF NOT EXISTS `LigneFraisForfait` (
  `quantite` int(11) DEFAULT NULL,
  `id_FraisForfait` char(3) NOT NULL,
  `id_FicheFrais` char(4) NOT NULL,
  PRIMARY KEY (`id_FraisForfait`,`id_FicheFrais`),
  FOREIGN KEY (`id_FraisForfait`) REFERENCES FraisForfait(`id_FraisForfait`),
  FOREIGN KEY (`id_FicheFrais`) REFERENCES FicheFrais(`id_FicheFrais`)
) ENGINE=InnoDB;

-- --------------------------------------------------------

--
-- Structure de la table `LigneFraisHorsForfait`
--

CREATE TABLE IF NOT EXISTS `LigneFraisHorsForfait` (
  `id_LigneFraisHorsForfait` int(11) NOT NULL auto_increment,
  `libelle` varchar(100) DEFAULT NULL,
  `date_` date DEFAULT NULL,
  `montant` decimal(10,2) DEFAULT NULL,
  `id_FicheFrais` char(4) NOT NULL,
  PRIMARY KEY (`id_LigneFraisHorsForfait`),
  FOREIGN KEY (`id_FicheFrais`) REFERENCES FicheFrais(`id_FicheFrais`)
) ENGINE=InnoDB;
