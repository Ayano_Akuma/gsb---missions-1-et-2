--
-- Contenu de la table `FraisForfait`
--
INSERT INTO `FraisForfait` (`id_FraisForfait`, `libelle`, `montant`) VALUES
('ETP', 'Forfait Etape', 110.00),
('KM', 'Frais Kilométrique', 0.62),
('NUI', 'Nuitée Hôtel', 80.00),
('REP', 'Repas Restaurant', 25.00);

-- --------------------------------------------------------

--
-- Contenu de la table `LigneFraisForfait`
--
INSERT INTO `lignefraisforfait` (`id_Fiche_Frais`, `id_Frais_Forfait`, `quantite`) VALUES
('AAA', 'NUI', 3),
('BBB', 'ETP', 2),
('CCC', 'NUI', 6),
('DDD', 'KM', 8),
('EEE', 'REP', 1),
('FFF', 'NUI', 4),
('GGG', 'ETP', 3),
('HHH', 'NUI', 3);

-- --------------------------------------------------------

--
-- Contenu de la table `Etat`
--
INSERT INTO `Etat` (`id_Etat`, `libelle`) VALUES
('RB', 'Remboursée'),
('CL', 'Saisie clôturée'),
('CR', 'Fiche créée, saisie en cours'),
('VA', 'Validée et mise en paiement');

-- --------------------------------------------------------

--
-- Contenu de la table `Comptable`
--
INSERT INTO `Comptable` (`id_Comptable`, `nom`, `prenom`, `login`, `mdp`, `adresse`, `cp`, `ville`, `dateEmbauche`) VALUES
('c111', 'Malou', 'Eddy', 'emalou', 'wb1op', '12 rue du Savoir', '93230', 'Romainville', '2003-06-20'),
('c222', 'Durif', 'Sylvain', 'sdurif', 'lm6er', '7 avenue Cosmique', '23120', 'GrandBourg', '2005-12-30'),
('c333', 'Sound', 'Pacific', 'psound', 'tz4yk', '38 allée Orange', '93100', 'Monteuil', '2005-05-05');

-- --------------------------------------------------------

--
-- Contenu de la table `Visiteur`
--
INSERT INTO `Visiteur` (`id_Visiteur`, `nom`, `prenom`, `login`, `mdp`, `adresse`, `cp`, `ville`, `dateEmbauche`, `id_Comptable`) VALUES
('a131', 'Villechalane', 'Louis', 'lvillachane', 'jux7g', '8 rue des Charmes', '46000', 'Cahors', '2005-12-21', 'c111'),
('a17', 'Andre', 'David', 'dandre', 'oppg5', '1 rue Petit', '46200', 'Lalbenque', '1998-11-23', 'c222'),
('a55', 'Bedos', 'Christian', 'cbedos', 'gmhxd', '1 rue Peranud', '46250', 'Montcuq', '1995-01-12', 'c333'),
('a93', 'Tusseau', 'Louis', 'ltusseau', 'ktp3s', '22 rue des Ternes', '46123', 'Gramat', '2000-05-01', 'c111'),
('b13', 'Bentot', 'Pascal', 'pbentot', 'doyw1', '11 allée des Cerises', '46512', 'Bessines', '1992-07-09', 'c222'),
('b16', 'Bioret', 'Luc', 'lbioret', 'hrjfs', '1 Avenue gambetta', '46000', 'Cahors', '1998-05-11', 'c333'),
('b19', 'Bunisset', 'Francis', 'fbunisset', '4vbnd', '10 rue des Perles', '93100', 'Montreuil', '1987-10-21', 'c111'),
('b25', 'Bunisset', 'Denise', 'dbunisset', 's1y1r', '23 rue Manin', '75019', 'Paris', '2010-12-05', 'c222'),
('b28', 'Cacheux', 'Bernard', 'bcacheux', 'uf7r3', '114 rue Blanche', '75017', 'Paris', '2009-11-12', 'c333'),
('b34', 'Cadic', 'Eric', 'ecadic', '6u8dc', '123 avenue de la République', '75011', 'Paris', '2008-09-23', 'c111'),
('b4', 'Charoze', 'Catherine', 'ccharoze', 'u817o', '100 rue Petit', '75019', 'Paris', '2005-11-12', 'c222'),
('b50', 'Clepkens', 'Christophe', 'cclepkens', 'bw1us', '12 allée des Anges', '93230', 'Romainville', '2003-08-11', 'c333'),
('b59', 'Cottin', 'Vincenne', 'vcottin', '2hoh9', '36 rue Des Roches', '93100', 'Monteuil', '2001-11-18', 'c111'),
('c14', 'Daburon', 'François', 'fdaburon', '7oqpv', '13 rue de Chanzy', '94000', 'Créteil', '2002-02-11', 'c222'),
('c3', 'De', 'Philippe', 'pde', 'gk9kx', '13 rue Barthes', '94000', 'Créteil', '2010-12-14', 'c333'),
('c54', 'Debelle', 'Michel', 'mdebelle', 'od5rt', '181 avenue Barbusse', '93210', 'Rosny', '2006-11-23', 'c111'),
('d13', 'Debelle', 'Jeanne', 'jdebelle', 'nvwqq', '134 allée des Joncs', '44000', 'Nantes', '2000-05-11', 'c222'),
('d51', 'Debroise', 'Michel', 'mdebroise', 'sghkb', '2 Bld Jourdain', '44000', 'Nantes', '2001-04-17', 'c333'),
('e22', 'Desmarquest', 'Nathalie', 'ndesmarquest', 'f1fob', '14 Place d Arc', '45000', 'Orléans', '2005-11-12', 'c111'),
('e24', 'Desnost', 'Pierre', 'pdesnost', '4k2o5', '16 avenue des Cèdres', '23200', 'Guéret', '2001-02-05', 'c222'),
('e39', 'Dudouit', 'Frédéric', 'fdudouit', '44im8', '18 rue de l église', '23120', 'GrandBourg', '2000-08-01', 'c333'),
('e49', 'Duncombe', 'Claude', 'cduncombe', 'qf77j', '19 rue de la tour', '23100', 'La souteraine', '1987-10-10', 'c111'),
('e5', 'Enault-Pascreau', 'Céline', 'cenault', 'y2qdu', '25 place de la gare', '23200', 'Gueret', '1995-09-01', 'c222'),
('e52', 'Eynde', 'Valérie', 'veynde', 'i7sn3', '3 Grand Place', '13015', 'Marseille', '1999-11-01', 'c333'),
('f21', 'Finck', 'Jacques', 'jfinck', 'mpb3t', '10 avenue du Prado', '13002', 'Marseille', '2001-11-10', 'c111'),
('f39', 'Frémont', 'Fernande', 'ffremont', 'xs5tq', '4 route de la mer', '13012', 'Allauh', '1998-10-01', 'c222'),
('f4', 'Gest', 'Alain', 'agest', 'dywvt', '30 avenue de la mer', '13025', 'Berre', '1985-11-01', 'c333');

-- --------------------------------------------------------

--
-- Contenu de la table `fichefrais`
--
INSERT INTO `fichefrais` (`id_Fiche_Frais`, `nbJustificatifs`, `montantValide`, `dateModif`, `id_Visiteur`, `id_Etat`, `Mois`) VALUES
('AAA', 2, '150.00', '2018-11-11', 'a17', 'CR', '201811'),
('BBB', 1, '250.00', '2018-10-10', 'a55', 'CL', '201810'),
('CCC', 4, '1478.00', '2018-11-11', 'c3', 'CL', '201809'),
('DDD', 3, '18452.00', '2018-10-18', 'a131', 'CL', '201810'),
('EEE', 8, '1542.00', '2018-10-10', 'b16', 'CL', '201811'),
('FFF', 3, '182.45', '2018-10-17', 'b4', 'CL', '201811'),
('GGG', 7, '1578.00', '2018-11-04', 'b13', 'CL', '201810'),
('HHH', 3, '859.00', '2018-10-09', 'b25', 'CL', '201810');

-- --------------------------------------------------------